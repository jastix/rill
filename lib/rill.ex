defmodule Rill do
  @moduledoc """
  Rill is an application to collect data about online streams.
  """

  @headers ["Client-ID": Application.get_env(:rill, :twitch_client_id),
            "Accept": "application/vnd.twitchtv.v5+json"]
  
  @doc """
  Main function for escript, which is used to parse command-line arguments
  and execute requests to Twitch API.

  """
  def main(args) do
    args |> parse_args |> process
  end

  def process([]), do: IO.puts(:stderr, "No arguments given")
  def process(options) do
    cond do
      options[:usernames] == nil ->
        IO.puts(:stderr, "Comma-separated list of usernames is expected")
      options[:with_user] && options[:with_stream] ->
        options
        |> get_users
        |> parse_users
        |> add_request_date_to_users
        |> print_users
        |> get_streams
        |> print_to_console
      options[:with_stream] ->
        options
        |> get_users
        |> parse_users
        |> add_request_date_to_users
        |> get_streams
        |> print_to_console
      true ->
        options |> get_users |> parse_users |> print_to_console
    end
  end

  def get_users(options) do
    usernames = options[:usernames] |> String.downcase()
    api_url = "https://api.twitch.tv/kraken/users?login=#{usernames}"
    case HTTPoison.get(api_url, @headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}} ->
        {headers, body}
      {:ok, %HTTPoison.Response{status_code: 400, body: body}} ->
        IO.puts(:stderr, body)
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.puts(:stderr, reason)
    end
  end

  def get_streams([]), do: []
  def get_streams(users) do
    channel_ids = extract_field("_id", users)
    for channel_id <- channel_ids, into: [] do
      Process.sleep(1000)
      channel_id
      |> get_stream_by_user
      |> parse_stream
      |> add_request_date_to_stream
    end
  end

  def get_stream_by_user(channel_id) do
    api_url = "https://api.twitch.tv/kraken/streams/#{channel_id}"
    case HTTPoison.get(api_url, @headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}} ->
        {headers, body}
      {:ok, %HTTPoison.Response{status_code: 400, body: body}} ->
        IO.puts(:stderr, body)
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.puts(:stderr, reason)
    end
  end

  defp parse_stream({headers, body}) do
    stream = body
    |> Poison.Parser.parse!
    |> Map.get("stream")

    {headers, stream}
  end

  defp add_request_date_to_stream({headers, stream}) do
    date = for { "Date", date } <- headers, into: "", do: date

    Map.put(stream, :request_date, date)
  end

  defp parse_users({headers, body}) do
    users = body
    |> Poison.Parser.parse!
    |> Map.get("users")

    {headers, users}
  end

  defp add_request_date_to_users({headers, users}) do
    date = for { "Date", date } <- headers, into: "", do: date
    users
    |> Enum.map(fn(user) -> Map.put(user, :request_date, date) end)
  end

  defp print_users([]), do: []
  defp print_users(users) do
    for user <- users, do: print_element(user)
    users
  end

  defp print_to_console([]), do: nil
  defp print_to_console([h | t]) do
    print_element(h)
    print_to_console(t)
  end

  defp print_element(el) do
    el
    |> Poison.encode!
    |> IO.puts
  end

  defp extract_field(field, source) do
    for element <- source, into: [], do: Map.get(element, field)
  end

  defp parse_args(args) do
    {options, _, _} = OptionParser.parse(args,
      strict: [usernames: :string, with_user: :boolean, with_stream: :boolean])
    options
  end

end
