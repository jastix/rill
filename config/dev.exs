use Mix.Config

config :rill, twitch_client_id: System.get_env("TWITCH_CLIENT_ID")
